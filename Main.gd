extends Node2D

var pillar_num = 0 
var score = 0
var frozen = false

func _ready():
	add_bird()
	add_pillar(randi()%500-250)

func _process(delta):
	if frozen == false:
		$ParallaxBackground.scroll_offset.x -= 100 * delta
		if $ParallaxBackground.scroll_offset.x <= -1024:
			$ParallaxBackground.scroll_offset.x = 0

func add_pillar(yposition):
	if pillar_num < 500 && frozen == false:
		var pillar0 = load("res://Pillar.tscn").instance()
		add_child(pillar0)
		pillar0.start(yposition)
		pillar_num += 1
	if pillar_num == 500:
		check_score()
		get_node("/root/Menu/Main").queue_free()

func add_bird():
	var bird = load("res://Player.tscn").instance()
	call_deferred("add_child", bird)
	bird.start()

func _on_PillarTimer_timeout():
	add_pillar(randi()%500-250)
	if pillar_num == 15  || pillar_num == 35 || pillar_num == 40:
		propagate_call("increase_gravity")

func game_over():
	get_node("/root/Menu/Music").stop()
	get_node("/root/Menu/Music").play(20)
	$HUD.show_message("Game Over!")
	$WaitTimer.start()

func _on_ScoreTimer_timeout():
	score += 1
	$HUD.update_score(score)

func _on_WaitTimer_timeout():
	get_node("/root/Menu/Music").stop()
	check_score()
	get_node("/root/Menu/Main").queue_free()

func check_score():
	if score > get_node("/root/Menu").score_array[9]:
		get_node("../").last_score = score
		get_node("../").call_save_page()
	else:
		get_node("/root/Menu/HBoxContainer").show()

func freeze():
	frozen = true