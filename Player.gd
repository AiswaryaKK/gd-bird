extends KinematicBody2D

export (int) var fly_speed
export (int) var gravity

var velocity = Vector2(0,0)
var running = false

func _ready():
#	set_physics_process(true)
	running = true
	$AnimatedSprite.play()
	get_node("/root/Menu/Main").frozen = false

func get_input():
	if Input.is_action_pressed("ui_up") && running == true:
		velocity.y = fly_speed
	else:
		velocity.y = gravity

func _physics_process(delta):
	velocity.x = 0
	get_input()
	position.x = 512
	move_and_collide(velocity * delta)

func hit():
	get_node("/root/Menu/Main").propagate_call("freeze")
	running = false
	$AnimatedSprite.stop()

func start():
	position = Vector2(512,75)

func increase_gravity():
	gravity += 50

func _on_VisibilityNotifier2D_viewport_exited(viewport):
	if running == true:
		call_deferred("free")
		set_physics_process(false)
		get_node("../").game_over()
