extends MarginContainer

# Called when the node enters the scene tree for the first time.
func _ready():
	$VBoxContainer/HBoxContainer2/Label.text = str(get_node("/root/Menu").last_score)
	$VBoxContainer/HBoxContainer2/LineEdit.grab_focus()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("ui_accept") || Input.is_action_pressed("ui_select"):
		validate_entry()

func validate_entry():
	get_node("/root/Menu").player_name = $VBoxContainer/HBoxContainer2/LineEdit.text
	get_node("/root/Menu").save_score()
	quit_to_menu()

func quit_to_menu():
	call_deferred("free")
	get_node("/root/Menu/HBoxContainer").show()
