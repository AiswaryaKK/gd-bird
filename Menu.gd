extends Node2D

var name_array = ["", "", "", "", "", "","","","",""]
var score_array = [0,0,0,0,0,0,0,0,0,0]
var score_file = "user://Highscores.txt"
var last_score = 0
var player_name = ""
var game_paused = false

func _ready():
	load_score()

func _process(delta):
	if Input.is_action_just_released("ui_accept") || Input.is_action_just_released("ui_select"):
		if game_paused == false:
			pause_game()
		else:
			unpause_game()

func pause_game():
	if has_node("/root/Menu/Main") == true:
		get_node("/root/Menu/Main/Node2D/Pause_label").show()
		$Music.stream_paused = true
		get_tree().paused = true
		set_deferred("game_paused", true)

func unpause_game():
	if has_node("/root/Menu/Main") == true:
		get_node("/root/Menu/Main/Node2D/Pause_label").hide()
		$Music.stream_paused = false
		get_tree().paused = false
		set_deferred("game_paused", false)


func _on_NewGame_pressed():
	$Music.play()
	var game = load("res://Main.tscn").instance()
	add_child(game)
	$HBoxContainer.hide()

func _on_HighScore_pressed():
	$HBoxContainer.hide()
	add_highscore_page()

func add_highscore_page():
	var page = load("res://HighscorePage.tscn").instance()
	add_child(page)

func call_save_page():
	var save_page = load("res://SavePage.tscn").instance()
	add_child(save_page)

func save_score():
	var i = 0
	for i in 10:
		if last_score <= score_array[i]:
			i += 1
		else:
			score_array.insert(i, last_score)
			name_array.insert(i, player_name)
			score_array.pop_back()
			name_array.pop_back()
			var dict = {
				"score_names":name_array,
				"scores":score_array
			}
			var f = File.new()
			f.open(score_file, File.WRITE)
			f.store_line(to_json(dict))
			f.close()
			break

func load_score():
	var f = File.new()
	if f.file_exists(score_file):
		f.open(score_file, File.READ)
		var dict = parse_json(f.get_as_text())
		f.close()
		name_array=dict["score_names"]
		score_array=dict["scores"]