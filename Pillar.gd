extends Area2D

var screen_pixel_size = 1024
var frozen = false

func _ready():
	pass 

func _process(delta):
	if frozen == false:
		position.x -= 200 * delta
		if position.x < -128:
			queue_free()

func start(yposition):
	position.y = yposition
	position.x = screen_pixel_size

func _on_Pillar_body_entered(body):
	if body.has_method("hit"):
		body.hit()
		get_node("/root/Menu/Main").game_over()

func clear():
	queue_free()

func freeze():
	frozen = true

