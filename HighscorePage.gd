extends Button

var score_label = load("res://ScoreLabel.tscn")
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	var position_container = $VBoxContainer/HBoxContainer/Position
	var name_container = $VBoxContainer/HBoxContainer/Name
	var score_container = $VBoxContainer/HBoxContainer/Score
	
	for i in get_node("/root/Menu").score_array.size():
		var position = score_label.instance()
		position.text = str(i+1)
		
		var name = score_label.instance()
		name.text = get_node("/root/Menu").name_array[i]
		
		var score = score_label.instance()
		score.text = str(get_node("/root/Menu").score_array[i])
		
		position_container.add_child(position)
		name_container.add_child(name)
		score_container.add_child(score)
	
func _on_HighscorePage_pressed():
	call_deferred("free")
	get_node("/root/Menu/HBoxContainer").show()

