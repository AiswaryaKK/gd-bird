extends Node2D

var current_scene = null

func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)

func goto_scene(path):
	call_deferred("_deferred_goto_scene", path)

func _deferred_goto_scene(path):
	current_scene.free()     # It is now safe to remove the current scene
	var s = ResourceLoader.load(path)     # Load the new scene.
	current_scene = s.instance()     # Instance the new scene.
	get_tree().get_root().add_child(current_scene)  # Add it to the active scene, as child of root.
	get_tree().set_current_scene(current_scene)     # Optionally, to make it compatible with the SceneTree.change_scene() API.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
